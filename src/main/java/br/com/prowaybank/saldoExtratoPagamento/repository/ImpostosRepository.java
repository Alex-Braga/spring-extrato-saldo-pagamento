package br.com.prowaybank.saldoExtratoPagamento.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.prowaybank.saldoExtratoPagamento.model.Impostos;

@Repository
public interface ImpostosRepository extends JpaRepository<Impostos, Integer> {

}
