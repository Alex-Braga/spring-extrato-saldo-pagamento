package br.com.prowaybank.saldoExtratoPagamento.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.prowaybank.saldoExtratoPagamento.model.Boleto;

@Repository
public interface BoletoRepository extends JpaRepository<Boleto, Integer> {
	
}
