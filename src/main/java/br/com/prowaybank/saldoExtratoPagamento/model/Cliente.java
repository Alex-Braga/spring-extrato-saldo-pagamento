package br.com.prowaybank.saldoExtratoPagamento.model;

public abstract class Cliente {
	
	protected int id;
	protected String nome;
	protected String chavePix;
	protected double saldo; 

	
	
	public int getId() {
		return id;
	}

	

	public void setId(int id) {
		this.id = id;
	}



	public String getNome() {
		return nome;
	}



	public void setNome(String nome) {
		this.nome = nome;
	}



	public String getChavePix() {
		return chavePix;
	}



	public void setChavePix(String chavePix) {
		this.chavePix = chavePix;
	}



	public double getSaldo() {
		return saldo;
	}



	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}



	public Cliente(int id, String nome, String chavePix, double saldo) {
		this.id = id;
		this.nome = nome;
		this.chavePix = chavePix;
		this.saldo = saldo;
	}



	
	
	public Cliente() {}

}
