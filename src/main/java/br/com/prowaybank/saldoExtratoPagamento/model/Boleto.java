package br.com.prowaybank.saldoExtratoPagamento.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Boleto {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id_boleto;
	private String cod_barras;
	private double valor;
	private String data_vencimento;
	private String beneficiario;
	private String servico;
	private String banco;
	private String agencia;
	private String conta;
	
	public Integer getId_boleto() {
		return id_boleto;
	}

	public void setId_boleto(Integer id_boleto) {
		this.id_boleto = id_boleto;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	/**
	 * método que retorna o código de barras
	 * @return
	 */
	public String getCod_barras() {
		return cod_barras;
	}

	/**
	 * método para inserir o código de barras
	 * @param cod_barras
	 */
	public void setCod_barras(String cod_barras) {
		this.cod_barras = cod_barras;
	}

	/**
	 * método que retorna o valor do boletp
	 * @return
	 */
	public double getValor() {
		return valor;
	}

	/**
	 * método para inserir o valor ao boleto
	 * @param valor
	 */
	public void setValor(double valor) {
		this.valor = valor;
	}

	/**
	 * método que retorna a data de vencimento
	 * @return
	 */
	public String getData_vencimento() {
		return data_vencimento;
	}

	/**
	 * método que insere o valor na data de vencimento
	 * @param data_vencimento
	 */
	public void setData_vencimento(String data_vencimento) {
		this.data_vencimento = data_vencimento;
	}

	/**
	 * método que retorna o nome do beneficiario
	 * @return
	 */
	public String getBeneficiario() {
		return beneficiario;
	}

	/**
	 * método para inserir o nome do beneficiario
	 * @param data_pagamento
	 */
	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}

	/**
	 * método que retorna o tipo do servico
	 * @return
	 */
	public String getServico() {
		return servico;
	}

	/**
	 * método para inserir o tipo de serviço
	 * @param data_pagamento
	 */
	public void setServico(String servico) {
		this.servico = servico;
	}

	/**
	 * método que retorna o numero da agencia
	 * @return
	 */
	public String getAgencia() {
		return agencia;
	}

	/**
	 * método para inserir o numero da agencia
	 * @param data_pagamento
	 */
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	/**
	 * método que retorna o numero da conta
	 * @return
	 */
	public String getConta() {
		return conta;
	}

	/**
	 * método para inserir o numero da conta
	 * @param data_pagamento
	 */
	public void setConta(String conta) {
		this.conta = conta;
	}

	/**
	 * método que retorna o tipo de operacao
	 * @return
	 */
	
	public Boleto() {
		
	}

	public Boleto(Integer id_boleto, String cod_barras, double valor, 
				  String data_vencimento, String beneficiario, 
				  String servico, String banco, String agencia, String conta) {
		
		this.id_boleto = id_boleto;
		this.cod_barras = cod_barras;
		this.valor = valor;
		this.data_vencimento = data_vencimento;
		this.beneficiario = beneficiario;
		this.servico = servico;
		this.banco = banco;
		this.agencia = agencia;
		this.conta = conta;
		
	}
}
