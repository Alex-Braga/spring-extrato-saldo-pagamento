package br.com.prowaybank.saldoExtratoPagamento.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

import br.com.prowaybank.saldoExtratoPagamento.interfaces.TipoImposto;

@Entity
public class Impostos extends Boleto implements TipoImposto{
	
	private TipoImposto tipoImposto;
	
	public Impostos(int id_boleto, String cod_barras, double valor, String data_vencimento, String data_pagamento,
			String hora_pagamento, String beneficiario, String servico, String CNPJ, String agencia, String conta,
			String operacao, double taxaJuros) {
	}
	
	public Impostos() {}
	
	
	public TipoImposto getTipoImposto() {
		return tipoImposto;
	}

	public void setTipoImposto(TipoImposto tipoImposto) {
		this.tipoImposto = tipoImposto;
	}
}
