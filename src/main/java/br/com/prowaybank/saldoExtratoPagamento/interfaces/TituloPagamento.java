package br.com.prowaybank.saldoExtratoPagamento.interfaces;

public interface TituloPagamento {

	public double getValor();
	public String getBeneficiario();
	public int getIdTitulo();

}
