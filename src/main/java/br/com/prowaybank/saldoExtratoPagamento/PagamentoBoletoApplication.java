package br.com.prowaybank.saldoExtratoPagamento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import br.com.prowaybank.saldoExtratoPagamento.controller.PagamentoBoletoController;

@SpringBootApplication
public class PagamentoBoletoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PagamentoBoletoApplication.class, args);
	}

}
