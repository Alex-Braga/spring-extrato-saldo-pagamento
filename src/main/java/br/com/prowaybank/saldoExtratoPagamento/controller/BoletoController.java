package br.com.prowaybank.saldoExtratoPagamento.controller;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.prowaybank.saldoExtratoPagamento.model.Boleto;
import br.com.prowaybank.saldoExtratoPagamento.repository.BoletoRepository;

@RestController
public class BoletoController {
	
	@Autowired
	private BoletoRepository boletoRepository;
	
	@GetMapping("/boletos")
	public List<Boleto> getAll(){
		return boletoRepository.findAll();
	}
	
	@GetMapping("/boletos/{id}")
	public Optional<Boleto> getById(@PathVariable("id") Integer id) {
		return boletoRepository.findById(id);
	}
	
	@PostMapping("/save")
	public Boleto create(@RequestBody Boleto newBoleto, Integer id) {
		boletoRepository.save(newBoleto);
		return newBoleto;
	}
	
}
