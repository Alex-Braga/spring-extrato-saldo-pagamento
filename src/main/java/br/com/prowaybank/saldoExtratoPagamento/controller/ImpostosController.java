package br.com.prowaybank.saldoExtratoPagamento.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.prowaybank.saldoExtratoPagamento.model.Impostos;
import br.com.prowaybank.saldoExtratoPagamento.repository.ImpostosRepository;

@RestController
public class ImpostosController {
	
	@Autowired
	ImpostosRepository impostosRepository;
	
	@GetMapping("/impostos")
	public List<Impostos> getAll(){
		return impostosRepository.findAll();
	}
	
	@GetMapping("/impostos/{id}")
	public Optional<Impostos> getById(@PathVariable("id") Integer id) {
		return impostosRepository.findById(id);
	}
	
	@PostMapping("/impostos")
	public Impostos create(@RequestBody Impostos newImpostos, Integer id) {
		impostosRepository.save(newImpostos);
		return newImpostos;
	}
	
}
